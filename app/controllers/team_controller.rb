# frozen_string_literal: true

require 'net/http'
require 'yaml'
require 'json'

TEAM_YML_URL = 'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml'
GEOCODE_URL  = 'https://maps.googleapis.com/maps/api/geocode/json'
TZ_URL       = 'https://maps.googleapis.com/maps/api/timezone/json'
GEOCODE_API_KEY = Rails.application.credentials.google[:api_key]

class Coordinates
  def initialize(lat, lng)
    @lat = lat
    @lng = lng
  end

  def to_s
    "#{@lat},#{@lng}"
  end
end

def get_team_data
  begin
    data_file = open('./team.yml', 'r')
    yaml = data_file.read
    puts yaml
    unless yaml.present?
      data_file.close
      raise
    end
    data_file.close
  rescue StandardError
    uri = URI(TEAM_YML_URL)
    yaml = Net::HTTP.get(uri) # => String
    data_file = open('./team.yml', 'w+')
    data = YAML.safe_load(yaml)
    data_file.write(data.to_yaml)
    data_file.close
  end

  data = YAML.safe_load(yaml)
  data
end

def get_cache
  begin
    cache_file = open('./cache.json', 'r')
    json = cache_file.read
    cache = JSON.parse(json)
    cache_file.close
  rescue StandardError
    cache = {}
  end
  cache['coordinates'] = {} unless cache['coordinates'].present?
  cache['tz'] = {} unless cache['tz'].present?

  cache
end

def set_cache(cache)
  cache_file = open('./cache.json', 'w+')
  cache_file.write(cache.to_json)
  cache_file.close
end

def get_coords(city, country)
  cache = get_cache

  query = city + ', ' + country
  coords = cache['coordinates'][query]
  return Coordinates.new(coords['lat'], coords['lng']) if coords.present?

  uri = URI(GEOCODE_URL)
  params = { address: query, key: GEOCODE_API_KEY }
  uri.query = URI.encode_www_form(params)
  res = Net::HTTP.get(uri)
  res_json = JSON.parse(res)
  location = res_json['results'][0]['geometry']['location']

  # cache it
  cache['coordinates'][query] = location
  set_cache(cache)

  Coordinates.new(location['lat'], location['lng'])
end

def get_timezone(coords)
  cache = get_cache

  query = coords.to_s
  tz = cache['tz'][query]
  return tz if tz.present?

  uri = URI(TZ_URL)
  params = { location: query, timestamp: Time.now.to_i, key: GEOCODE_API_KEY }
  uri.query = URI.encode_www_form(params)
  res = Net::HTTP.get(uri)
  res_json = JSON.parse(res)
  tz = res_json

  # cache it
  cache['tz'][query] = tz
  set_cache(cache)

  tz
end

def get_local_time(tz)
  Time.now.in_time_zone(tz['timeZoneId'])
end

def is_awake(time)
  time.hour >= 8 && time.hour <= 19
end

class TeamController < ApplicationController
  def index
    data = get_team_data
    data_filtered = []

    data.each do |team_member|
      project = team_member['projects']
      next unless project.present?

      gitlab_project = project['gitlab']
      next unless gitlab_project.present?

      data_filtered.push team_member if gitlab_project == 'maintainer frontend'
    end

    data_filtered.each do |team_member|
      coords = get_coords(team_member['locality'], team_member['country'])
      tz = get_timezone(coords)
      local_time = get_local_time(tz)
      team_member['local_time'] = local_time
      team_member['is_awake'] = is_awake(local_time)
    end

    data_filtered = data_filtered.sort_by { |obj| obj['is_awake'] ? 0 : 1 }

    @team = data_filtered
  end
end
